package com.example.jimmy.mycalendardemo.cal.base;

public class CellConfig {

    public static float cellWidth = 100;
    public static float cellHeight = 100;

    // 中间页的位置
    public static int middlePosition = 500;
    // 下面两个:
    // 点击了收缩展开后，记录当前页的位置，后期换算位移用他俩
    public static int Month2WeekPos = 500;
    public static int Week2MonthPos = 500;
    // 下面两个:
    // 一开始只设置了一个，后来发现数据有交差（在使用前就做了更改）
    // 所以分成了两个
    public static DateData w2mPointDate;
    public static DateData m2wPointDate;

    public static DateData weekAnchorPointDate;

    //最小日期
    public static DateData minDate;
    //最大日期
    public static DateData maxDate;
    //不可选日期
    public static DateData[] disableDate;

    //是否显示节气
    public static boolean isShowSeasonText;

}
