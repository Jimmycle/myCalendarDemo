package com.example.jimmy.mycalendardemo.cal.util;

import com.example.jimmy.mycalendardemo.cal.base.DateData;

import java.util.Calendar;

public class CurrentCalendar {

    public static DateData getCurrentDateData() {
        Calendar calendar = Calendar.getInstance();
        return new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

}
