package com.example.jimmy.mycalendardemo.cal.listeners;

import android.view.View;

import com.example.jimmy.mycalendardemo.cal.base.DateData;

/**
 * 点击选中日期监听
 * 需要在ExpCalendarView实现该接口,获取监听内容
 */
public abstract class OnDateClickListener {

    public static OnDateClickListener instance;

    /**
     * @param view        视图
     * @param date        天数
     * @param canSelected 是否能被选在的日期
     */
    public abstract void onDateClick(View view, DateData date, boolean canSelected);
}
