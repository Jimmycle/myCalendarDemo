package com.example.jimmy.mycalendardemo.cal.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.example.jimmy.mycalendardemo.ExpCalendarView;
import com.example.jimmy.mycalendardemo.cal.base.DateData;
import com.example.jimmy.mycalendardemo.cal.fragment.MonthExpFragment;


public class CalendarViewExpAdapter extends FragmentStatePagerAdapter {

    private DateData date;
    private int dateCellId;
    private int markCellId;

    public CalendarViewExpAdapter(FragmentManager fm) {
        super(fm);
    }

    public CalendarViewExpAdapter setDate(DateData date) {
        this.date = date;
        return this;
    }

    public CalendarViewExpAdapter setDateCellId(int dateCellRes) {
        this.dateCellId = dateCellRes;
        return this;
    }


    public CalendarViewExpAdapter setMarkCellId(int markCellId) {
        this.markCellId = markCellId;
        return this;
    }

    @Override
    public Fragment getItem(int position) {
        MonthExpFragment fragment = new MonthExpFragment();
        fragment.setData(position, dateCellId, markCellId, date.getCurrentDateSpecialName());
        return fragment;
    }

    @Override
    public int getCount() {
        return 1000;
    }


    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        ((ExpCalendarView) container).measureCurrentView();
    }

    /*
     * 重写该方法，为了刷新页面
     */
    @Override
    public int getItemPosition(Object object) {
        if (object.getClass().getName().equals(MonthExpFragment.class.getName())) {
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

}
