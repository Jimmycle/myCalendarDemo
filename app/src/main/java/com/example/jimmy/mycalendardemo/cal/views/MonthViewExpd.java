package com.example.jimmy.mycalendardemo.cal.views;

import android.content.Context;
import android.util.AttributeSet;

import com.example.jimmy.mycalendardemo.cal.adapter.CalendarExpAdapter;
import com.example.jimmy.mycalendardemo.cal.base.MonthWeekData;


public class MonthViewExpd extends MonthView {

    private MonthWeekData monthWeekData;
    private CalendarExpAdapter adapter;

    public MonthViewExpd(Context context) {
        super(context);
    }

    public MonthViewExpd(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initMonthAdapter(int pagePosition, int cellView, int markView, String specialName) {
        getMonthWeekData(pagePosition, specialName);
        adapter = new CalendarExpAdapter(getContext(), 1, monthWeekData.getData()).setCellViews(cellView, markView);
        this.setAdapter(adapter);
    }

    private void getMonthWeekData(int position, String specialName) {
        monthWeekData = new MonthWeekData(position, specialName);
    }

}
