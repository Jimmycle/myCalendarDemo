package com.example.jimmy.mycalendardemo.cal.base;

import java.util.ArrayList;
import java.util.Calendar;

public class MonthWeekData {
    private Calendar calendar;

    private int realPosition;
//    private int weekIndex, preNumber, afterNumber;

    private ArrayList<DayData> weekContent;

    /*
     * 绝对位置
     */
    public MonthWeekData(int position, String specialName) {
        realPosition = position;
        calendar = Calendar.getInstance();
        DateData today = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        if (CellConfig.m2wPointDate == null) {
            CellConfig.m2wPointDate = today;
        }
        if (CellConfig.w2mPointDate == null) {
            CellConfig.w2mPointDate = today;
        }
        if (CellConfig.weekAnchorPointDate == null) {
            CellConfig.weekAnchorPointDate = today;
        }
        initWeekArray(specialName);

    }

    /*
     * Week2MonthPos 与 Month2WeekPos 是关键。
     */
    private void initWeekArray(String specialName) {
        weekContent = new ArrayList<>();

        // 下面是：获得上次记录的位置，根据页数位移，判断该页面的锚点（该页是几月份）
        calendar.set(CellConfig.m2wPointDate.getYear(), CellConfig.m2wPointDate.getMonth() - 1, CellConfig.m2wPointDate.getDay());
        if (CellConfig.Week2MonthPos != CellConfig.Month2WeekPos) {
            // 中间页面与今天的相对页数差
            int distance = CellConfig.Month2WeekPos - CellConfig.Week2MonthPos;
            // 滚到当前页
            calendar.add(Calendar.MONTH, distance);
        }
        // 如果是今天的月份，则锚点的日期为今天； 如果不是今天的月份，则锚点的日期为1号
        calendar.set(Calendar.DAY_OF_MONTH, getAnchorDayOfMonth(CellConfig.weekAnchorPointDate));
        // 下面是：获得该页的锚点后，判断三页显示的内容，中间和两边页显示不同
        if (realPosition == CellConfig.Month2WeekPos) {
        } else {
            calendar.add(Calendar.DATE, (realPosition - CellConfig.Month2WeekPos) * 7);
        }

        // 记录中间页的pointDate
        if (realPosition == CellConfig.middlePosition) {
            CellConfig.w2mPointDate = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        }

        // 添加数据
        DayData addDate;
        int weekIndex = calendar.get(Calendar.DAY_OF_WEEK);
//        calendar.add(Calendar.DATE, -weekIndex + 1);  //默认从星期日作为开始
        //需求:从星期一开始计算
        int moveIndex = -weekIndex + 2;
        if (weekIndex == 1) { //当前是星期日,发生后移处理
            moveIndex -= 7;
        }
        calendar.add(Calendar.DATE, moveIndex);
        for (int i = 0; i < 7; i++) {
            DateData dateData = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            dateData.setCurrentDateSpecialName(specialName);
            addDate = new DayData(dateData);
            weekContent.add(addDate);
            calendar.add(Calendar.DATE, 1);
        }
    }

    private int getAnchorDayOfMonth(DateData date) {
        int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
        int month = date.getMonth() - 1;
        int selectedMonth = calendar.get(Calendar.MONTH);
        if (selectedMonth == month && calendar.get(Calendar.YEAR) == date.getYear()) {
            return date.getDay();
        }

        if (selectedMonth == thisMonth && month != thisMonth) {
            Calendar calendar = Calendar.getInstance();
            return calendar.get(Calendar.DAY_OF_MONTH);
        }

        return 1;
    }

    public ArrayList getData() {
        return weekContent;
    }

}
