package com.example.jimmy.mycalendardemo;

import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jimmy on 2018/5/22.
 */
public class WaterImageManager {
    private static final String TAG = "SlideActivity";
    private List<WaterImage> breathingLampImages = new ArrayList<>();
    private List<WaterImage> slideImages = new ArrayList<>();
    public static final int BREATHING_LAMP_PERIOD_TIME = 1500 / 50;

    public WaterImageManager() {
        for (int i = 0; i <= 50; i++) {
            String imageStart;
            if (i < 10) {
                imageStart = "s__0000" + i;
            } else {
                imageStart = "s__000" + i;
            }
            WaterImage waterImage = new WaterImage();
            waterImage.setImageResId(getImageId(imageStart));
            waterImage.setOffSet(BREATHING_LAMP_PERIOD_TIME);
            breathingLampImages.add(waterImage);
        }
        for (int i = 0; i <= 50; i++) {
            String imageName;
            if (i < 10) {
                imageName = "td__0000" + i;
            } else {
                imageName = "td__000" + i;
            }
            WaterImage waterImage = new WaterImage();
            waterImage.setImageResId(getImageId(imageName));
//            float offSet = asc(100, 50, i);
            float offSet = speed(100, 50, i);
            Log.d(TAG, "offSet==" + offSet);
            waterImage.setOffSet(offSet);
            slideImages.add(waterImage);
        }
        Log.d(TAG, "slideImages==" + slideImages);
    }

    /**
     * 匀加速公式: S = 0.5at^2
     */
    public float asc(float distance, float times, int currentTime) {
        float a = 2 * distance / (times * times);
        return (float) (0.5 * a * currentTime * currentTime);
    }

    /**
     * 阶段处理
     *
     * @return
     */
    public float speed(float distance, float times, int currentTime) {
        float startTime = times * 0.2f;
        float midTime = times * 0.8f;
        float endTime = times * 1.0f;
        float startDistance = distance * 0.1f;
        float midDistance = distance * 0.9f;
        float endDistance = distance * 1.0f;
        if (currentTime >= 0 && currentTime <= startTime) {
            return getOffSet(0, startDistance, 0, startTime, currentTime);
        } else if (currentTime > startTime && currentTime <= midTime) {
            return getOffSet(startDistance, midDistance, startTime, midTime, currentTime);
        } else if (currentTime > midTime && currentTime <= endTime) {
            return getOffSet(midDistance, endDistance, midTime, endTime, currentTime);
        }
        return 0.0f;
    }

    public float getOffSet(float startDistance, float endDistance, float startTime, float endTime, float currentTime) {
        float r = (endDistance - startDistance) / (endTime - startTime);
        return startDistance + (currentTime - startTime) * r;
    }

    public List<WaterImage> getBreathingLampImages() {
        Log.d(TAG, "breathingLampImages==" + breathingLampImages);
        return breathingLampImages;
    }

    public List<WaterImage> getSlideImages() {
        Log.d(TAG, "slideImages==" + slideImages);
        return slideImages;
    }

    /**
     * 根据index获取呼吸灯图片
     *
     * @param i
     * @return
     */
    public int getBreathingLampImage(int i) {
        if (i >= breathingLampImages.size()) {
            i = i % breathingLampImages.size();
        }
        WaterImage waterImage = breathingLampImages.get(i);
        return waterImage.getImageResId();
    }

    /**
     * 根据offSet获取图片resId
     *
     * @param offset
     * @return
     */
    public int getCurrentImageResIdByOffSet(float offset) {
        int percent = (int) (offset * 100);
        Log.d(TAG, "percent==" + percent);
        for (int i = 0; i < slideImages.size(); i++) {
            WaterImage waterImage = slideImages.get(i);
            if ((int) waterImage.getOffSet() >= percent) {
                return waterImage.getImageResId();
            }
        }
        return 0;
    }

    /**
     * 获取图片名称获取图片的资源id的方法
     *
     * @param imageName
     * @return
     */
    public int getImageId(String imageName) {
        Class mipmapClass = R.mipmap.class;
        int r_id;
        try {
            Field field = mipmapClass.getField(imageName);
            r_id = field.getInt(field.getName());
        } catch (Exception e) {
            r_id = R.mipmap.ic_launcher;
        }
        return r_id;
    }

    public class WaterImage {
        public int imageResId;
        public float offSet;

        public WaterImage() {
        }

        public WaterImage(int imageResId, float offSet) {
            this.imageResId = imageResId;
            this.offSet = offSet;
        }

        public int getImageResId() {
            return imageResId;
        }

        public void setImageResId(int imageResId) {
            this.imageResId = imageResId;
        }

        public float getOffSet() {
            return offSet;
        }

        public void setOffSet(float offSet) {
            this.offSet = offSet;
        }

        @Override
        public String toString() {
            return "WaterImage{" +
                    "imageResId=" + Integer.toHexString(imageResId) +
                    ", offSet=" + offSet +
                    '}';
        }
    }
}
