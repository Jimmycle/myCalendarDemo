package com.example.jimmy.mycalendardemo.cal.base;

import android.text.TextUtils;

public class DayData {

    private DateData date;
    private int textColor;
    private int textSize;

    public DayData(DateData date) {
        this.date = date;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public String getText() {
        if (!TextUtils.isEmpty(date.getCurrentDateSpecialName())) {
            return "" + date.getCurrentDateSpecialName();
        } else {
            return "" + date.getDay();
        }
    }

    public String getSeasonText() {
        return date.getSolarTermName();
    }

    public DateData getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "DayData{" +
                "date=" + date +
                ", textColor=" + textColor +
                ", textSize=" + textSize +
                '}';
    }
}
