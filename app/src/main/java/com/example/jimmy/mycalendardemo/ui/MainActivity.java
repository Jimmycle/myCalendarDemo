package com.example.jimmy.mycalendardemo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.jimmy.mycalendardemo.R;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView list = findViewById(R.id.list);
        String[] data = new String[]{"日历页面", "侧滑动画"};
        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data));
        list.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                startActivity(new Intent(this, CalendarActivity.class));
                break;
            case 1:
                startActivity(new Intent(this, SlideActivity.class));
                break;
            default:
                break;
        }
    }
}
