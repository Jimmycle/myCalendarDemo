package com.example.jimmy.mycalendardemo.cal.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.jimmy.mycalendardemo.cal.base.CellConfig;
import com.example.jimmy.mycalendardemo.cal.base.MarkStyle;
import com.example.jimmy.mycalendardemo.cal.base.DayData;
import com.example.jimmy.mycalendardemo.cal.base.MarkedDates;
import com.example.jimmy.mycalendardemo.cal.listeners.OnDateClickListener;
import com.example.jimmy.mycalendardemo.cal.views.BaseCellView;
import com.example.jimmy.mycalendardemo.cal.views.BaseMarkView;
import com.example.jimmy.mycalendardemo.cal.views.DefaultCellView;
import com.example.jimmy.mycalendardemo.cal.views.DefaultMarkView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class CalendarAdapter extends ArrayAdapter implements Observer {
    private ArrayList data;
    private int cellView = -1;
    private int markView = -1;

    public CalendarAdapter(Context context, int resource, ArrayList data) {
        super(context, resource);
        this.data = data;
        MarkedDates.getInstance().addObserver(this);
    }

    public CalendarAdapter setCellViews(int cellView, int markView) {
        this.cellView = cellView;
        this.markView = markView;
        return this;
    }


    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View ret = null;
        DayData dayData = (DayData) data.get(position);
        MarkStyle style = MarkedDates.getInstance().check(dayData.getDate());
        boolean marked = style != null;
        if (marked) {
            dayData.getDate().setMarkStyle(style);
            if (markView > 0) {
                BaseMarkView baseMarkView = (BaseMarkView) View.inflate(getContext(), markView, null);
                baseMarkView.setDisplayText(dayData);
                ret = baseMarkView;
            } else {
                ret = new DefaultMarkView(getContext());
                ((DefaultMarkView) ret).setDisplayText(dayData);
            }
        } else {
            if (cellView > 0) {
                BaseCellView baseCellView = (BaseCellView) View.inflate(getContext(), cellView, null);
                baseCellView.setDisplayText(dayData);
                ret = baseCellView;
            } else {
                ret = new DefaultCellView(getContext());
                ((DefaultCellView) ret).setDisplayText(dayData);
            }
        }
        ((BaseCellView) ret).setDate(dayData.getDate());
        if (OnDateClickListener.instance != null) {
            if ((CellConfig.minDate == null || dayData.getDate().compareTo(CellConfig.minDate) > 0) &&
                    (CellConfig.maxDate == null || dayData.getDate().compareTo(CellConfig.maxDate) < 0)) {
                ((BaseCellView) ret).setOnDateClickListener(OnDateClickListener.instance, true);
            } else {
                ((BaseCellView) ret).setOnDateClickListener(OnDateClickListener.instance, false);
            }
        }
        return ret;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public void update(Observable observable, Object data) {
        this.notifyDataSetChanged();
    }
}
