package com.example.jimmy.mycalendardemo.cal.views;

import android.content.Context;
import android.util.AttributeSet;

import com.example.jimmy.mycalendardemo.cal.base.DateData;
import com.example.jimmy.mycalendardemo.cal.listeners.OnDateClickListener;


public abstract class BaseMarkView extends BaseCellView {
    private OnDateClickListener clickListener;
    private DateData date;

    public BaseMarkView(Context context) {
        super(context);
    }

    public BaseMarkView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
