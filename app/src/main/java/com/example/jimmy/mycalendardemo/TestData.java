package com.example.jimmy.mycalendardemo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * 测试数据
 */
public class TestData {

    public static ArrayList<ArrangeModel> initAmData() {
        ArrayList<ArrangeModel> test = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 8);
        c.set(Calendar.MINUTE, 15);
        String format;

        for (int i = 0; i < 14; i++) {
            c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 15);
            if (c.get(Calendar.HOUR) > 9) {
                if (c.get(Calendar.MINUTE) > 9) {
                    format = "%d:%d";
                } else {
                    format = "%d:0%d";
                }
            } else {
                if (c.get(Calendar.MINUTE) > 9) {
                    format = "0%d:%d";
                } else {
                    format = "0%d:0%d";
                }
            }
            String time = String.format(Locale.getDefault(), format, c.get(Calendar.HOUR), c.get(Calendar.MINUTE));
            ArrangeModel model;
            if (i == 13) {
                model = new ArrangeModel(time, 0, "闲");
            } else if (i == 10) {
                model = new ArrangeModel(time, 1, "满");
            } else {
                model = new ArrangeModel(time, 0, "");
            }
            test.add(model);
        }
        return test;
    }

    public static ArrayList<ArrangeModel> initPmData() {
        ArrayList<ArrangeModel> test = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 12);
        c.set(Calendar.MINUTE, 0);
        String format;

        for (int i = 0; i < 14; i++) {
            c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 15);
            if (c.get(Calendar.HOUR) > 9) {
                if (c.get(Calendar.MINUTE) > 9) {
                    format = "%d:%d";
                } else {
                    format = "%d:0%d";
                }
            } else {
                if (c.get(Calendar.MINUTE) > 9) {
                    format = "0%d:%d";
                } else {
                    format = "0%d:0%d";
                }
            }
            String time = String.format(Locale.getDefault(), format, c.get(Calendar.HOUR), c.get(Calendar.MINUTE));
            ArrangeModel model;
            if (i == 3) {
                model = new ArrangeModel(time, 0, "闲");
            } else if (i == 5) {
                model = new ArrangeModel(time, 1, "满");
            } else {
                model = new ArrangeModel(time, 0, "");
            }
            test.add(model);
        }
        return test;
    }
}
