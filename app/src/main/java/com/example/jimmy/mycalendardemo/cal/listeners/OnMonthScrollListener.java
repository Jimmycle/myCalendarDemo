package com.example.jimmy.mycalendardemo.cal.listeners;

import android.support.v4.view.ViewPager;

import com.example.jimmy.mycalendardemo.cal.base.CellConfig;
import com.example.jimmy.mycalendardemo.cal.base.DateData;
import com.example.jimmy.mycalendardemo.cal.util.ExpCalendarUtil;

import java.util.Calendar;

/**
 * 滑动日期监听
 * 需要在ExpCalendarView实现该接口,获取监听内容
 */
public abstract class OnMonthScrollListener implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        onMonthScroll(positionOffset);
    }

    @Override
    public void onPageSelected(int position) {
        CellConfig.middlePosition = position;
        DateData date = ExpCalendarUtil.position2Week(position);
        Calendar instance = Calendar.getInstance();
        instance.set(date.getYear(), date.getMonth(), date.getDay());
        onMonthChange(date.getYear(), date.getMonth(), date.getDay(), instance.get(Calendar.DAY_OF_WEEK_IN_MONTH));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * @param year  年
     * @param month 月
     * @param day   日
     * @param week  一个月中的第几个星期
     */
    public abstract void onMonthChange(int year, int month, int day, int week);

    /**
     * 滑动日历元素时的位置偏移像素获取
     *
     * @param positionOffset
     */
    public abstract void onMonthScroll(float positionOffset);

}
