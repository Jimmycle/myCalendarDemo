package com.example.jimmy.mycalendardemo.ui;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jimmy.mycalendardemo.ArrangeModel;
import com.example.jimmy.mycalendardemo.ExpCalendarView;
import com.example.jimmy.mycalendardemo.GridAdapter;
import com.example.jimmy.mycalendardemo.R;
import com.example.jimmy.mycalendardemo.TestData;
import com.example.jimmy.mycalendardemo.WeekColumnView;
import com.example.jimmy.mycalendardemo.cal.base.DateData;
import com.example.jimmy.mycalendardemo.cal.listeners.OnDateClickListener;
import com.example.jimmy.mycalendardemo.cal.listeners.OnMonthScrollListener;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * 主页面Demo
 * 结合WeekColumnView+ExpCalendarView的使用
 */
public class CalendarActivity extends AppCompatActivity {
    private static final String TAG = "CalendarActivity";
    private RelativeLayout back;
    private TextView tvTitle;
    private TextView tvYearMonth;
    private WeekColumnView weekColumnView;
    private ExpCalendarView expCalendarView;
    private FrameLayout preMonth;
    private FrameLayout nextMonth;
    private CardView preWeek;
    private CardView nextWeek;

    private GridView gridView;
    private RadioButton rbAm;
    private RadioButton rbPm;
    private RadioGroup rp;

    private GridAdapter adapter;
    ArrayList<ArrangeModel> tests = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        back = (RelativeLayout) findViewById(R.id.rl_back);
        tvTitle = (TextView) findViewById(R.id.title);
        tvYearMonth = (TextView) findViewById(R.id.main_YYMM_Tv);
        weekColumnView = (WeekColumnView) findViewById(R.id.week_column);
        expCalendarView = (ExpCalendarView) findViewById(R.id.calendar_exp);
        preMonth = (FrameLayout) findViewById(R.id.ll_pre_month);
        nextMonth = (FrameLayout) findViewById(R.id.ll_next_month);
        preWeek = (CardView) findViewById(R.id.cv_pre_week);
        nextWeek = (CardView) findViewById(R.id.cv_next_week);
        gridView = (GridView) findViewById(R.id.gv);
        rbAm = (RadioButton) findViewById(R.id.tab_am);
        rbPm = (RadioButton) findViewById(R.id.tab_pm);
        rp = (RadioGroup) findViewById(R.id.tab);

        //设置顶部标题
        tvTitle.setText(R.string.title_name);

        initWeekColumnView();
        initCalendarView();


        /**
         * 注册月份滚动监听
         */
        expCalendarView.setOnMonthScrollListener(new OnMonthScrollListener() {
            @Override
            public void onMonthChange(int year, int month, int date, int week) {
                Log.d(TAG, "，year:" + year + "，month:" + month + "，date:" + date + "，week:" + week);
            }

            @Override
            public void onMonthScroll(float positionOffset) {
            }
        });

        /**
         * 注册日期选择监听
         */
        expCalendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date, boolean canSelected //是否可以进行选择，由设置最大最小值决定
            ) {
                if (canSelected) {
                    expCalendarView.getMarkedDates().removeAdd();
                    expCalendarView.markDate(date); //只标记选中日期
                    expCalendarView.setMarkedTextColor(date, Color.WHITE);
                    //初始化背景渐变色
                    int[] colors = {0xff2DE9FF, 0xff0BABFE}; //渐变色
                    expCalendarView.setMarkedGradientColor(date, GradientDrawable.Orientation.TOP_BOTTOM, colors);
                } else {
                    Toast.makeText(getApplicationContext(), "不能选择该日期", Toast.LENGTH_SHORT).show();
                }
            }
        });

        preMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToPreMonth(); //上月
            }
        });

        nextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToNextMonth();  //下月
            }
        });

        preWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToPreWeek(); //上周
            }
        });

        nextWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToNextWeek(); //下周
            }
        });


        rbAm.setChecked(true); //default check
        adapter = new GridAdapter(this, tests);
        gridView.setAdapter(adapter);
        loadAm();
        rp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_am) {
                    loadAm();
                } else if (checkedId == R.id.tab_pm) {
                    loadPm();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    /**
     * 初始化日历控件标题栏
     */
    public void initWeekColumnView() {
        weekColumnView.setColumnBackgroundColor(Color.WHITE); //背景色，不设置有默认值
        weekColumnView.setColumnTextColor(0xFF323232); //字体颜色，不设置有默认值
        weekColumnView.setColumnWeekendTextColor(0xFFFFC514);//周末颜色，不设置有默认值
        weekColumnView.setColumnNames(getResources().getStringArray(R.array.week_titles)); //初始化标题内容，不设置有默认值
    }

    /**
     * 初始化日历控件
     */
    public void initCalendarView() {
        Calendar calendar = Calendar.getInstance();
        //绑定月视图,内容实时更新，如果不绑定月视图不会更新
        expCalendarView.bindUpdateTextView(tvYearMonth);
        //当前date显示名字修改
        expCalendarView.setCurrentDateSpecialName("今");
        //选中项设置,当天之后
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        DateData markDate = expCalendarView.markDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        //初始化选中颜色和背景色
//        expCalendarView.setMarkedColor(markDate, Color.parseColor("#FF2DE9FF"));
        expCalendarView.setMarkedTextColor(markDate, Color.WHITE);
        //初始化背景渐变色
        int[] colors = {0xff2DE9FF, 0xff0BABFE}; //渐变色
        expCalendarView.setMarkedGradientColor(markDate, GradientDrawable.Orientation.TOP_BOTTOM, colors);
        //设置默认字体
        expCalendarView.setDefaultTextColor(markDate, 0xFF323232);
        //设置周末字体颜色
        expCalendarView.setWeekendTextColor(markDate, 0xFFFFC514);
        //设置不可点击字体颜色
        expCalendarView.setDisableTextColor(markDate, 0xFF989898);
        //设置最小日期
//        calendar.add(Calendar.DAY_OF_MONTH, -2);
        expCalendarView.setMinDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        //设置最大日期
//        expCalendarView.setMaxDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, calendar.get(Calendar.DAY_OF_MONTH));
        //设置不可选日期
//        DateData date1 = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, 1);
//        DateData date2 = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, 3);
//        DateData date3 = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, 5);
//        expCalendarView.setDisableDate(date1, date2, date3);
        //禁止向上返回的时间
        calendar = Calendar.getInstance();
        expCalendarView.setLimitBackDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        //是否打开节气显示，默认关闭
        expCalendarView.setSeasonTextShow(true);
    }

    /**
     * 加载上午数据
     */
    private void loadAm() {
        ArrayList<ArrangeModel> data = TestData.initAmData();
        tests.clear();
        tests.addAll(data);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClick(new GridAdapter.OnItemClick() {
            @Override
            public void onClick(View convertView, int position, String times, int status, String state) {
                Log.d(TAG, "，position:" + position + "，times:" + times + "，status:" + status + "，state:" + state);
                adapter.setSelection(position);
            }
        });
    }

    /**
     * 加载下午数据
     */
    private void loadPm() {
        ArrayList<ArrangeModel> data = TestData.initPmData();
        tests.clear();
        tests.addAll(data);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClick(new GridAdapter.OnItemClick() {
            @Override
            public void onClick(View convertView, int position, String times, int status, String state) {
                Log.d(TAG, "，position:" + position + "，times:" + times + "，status:" + status + "，state:" + state);
                adapter.setSelection(position);
            }
        });
    }

}
