package com.example.jimmy.mycalendardemo.cal.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jimmy.mycalendardemo.cal.base.MarkStyle;
import com.example.jimmy.mycalendardemo.cal.base.DayData;
import com.example.jimmy.mycalendardemo.cal.base.CellConfig;

/**
 * 默认绘制选中日历格视图
 */
public class DefaultMarkView extends BaseMarkView {
    private TextView textView;
    private TextView textViewSeason;
    private AbsListView.LayoutParams matchParentParams;
    private int orientation;

    private View sideBar;
    private TextView markTextView;
    private ShapeDrawable circleDrawable;
    private GradientDrawable gradientDrawable;

    public DefaultMarkView(Context context) {
        super(context);
    }

    public DefaultMarkView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initLayoutWithStyle(MarkStyle style) {
        textView = new TextView(getContext());
        textView.setGravity(Gravity.CENTER);
        matchParentParams = new AbsListView.LayoutParams((int) CellConfig.cellWidth, (int) CellConfig.cellHeight);
        switch (style.getStyle()) {
            case MarkStyle.DEFAULT:
                this.setLayoutParams(matchParentParams);
                this.setOrientation(VERTICAL);
                this.setPadding(12, 12, 12, 12);
                if (!CellConfig.isShowSeasonText) {
                    textView = new TextView(getContext());
                    textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    circleDrawable = new ShapeDrawable(new OvalShape());
                    circleDrawable.getPaint().setColor(style.getColor());
                    textView.setBackgroundDrawable(circleDrawable);
                    this.addView(textView);
                } else {
                    LinearLayout container = new LinearLayout(getContext());
                    container.setOrientation(VERTICAL);
                    container.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    container.setGravity(Gravity.CENTER);
                    textView = new TextView(getContext());
                    textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    textView.setTextColor(style.getMarkedTextColor());
                    container.addView(textView);
                    textViewSeason = new TextView(getContext());
                    LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, -6, 0, 0);
                    textViewSeason.setLayoutParams(lp);
                    textViewSeason.setTextColor(style.getMarkedTextColor());
                    textViewSeason.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
                    container.addView(textViewSeason);
                    circleDrawable = new ShapeDrawable(new OvalShape());
                    circleDrawable.getPaint().setColor(style.getColor());
                    container.setBackgroundDrawable(circleDrawable);
                    this.addView(container);
                }
                return;
            case MarkStyle.Gradient_BACKGROUND:
                this.setLayoutParams(matchParentParams);
                this.setOrientation(VERTICAL);
                this.setPadding(12, 12, 12, 12);
                if (!CellConfig.isShowSeasonText) {
                    textView = new TextView(getContext());
                    textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    gradientDrawable = new GradientDrawable(style.getOrientation(), style.getGradientColors());
                    gradientDrawable.setShape(GradientDrawable.OVAL);
                    textView.setBackgroundDrawable(gradientDrawable);
                    this.addView(textView);
                } else {
                    LinearLayout container = new LinearLayout(getContext());
                    container.setOrientation(VERTICAL);
                    container.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    container.setGravity(Gravity.CENTER);
                    textView = new TextView(getContext());
                    textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    textView.setTextColor(style.getMarkedTextColor());
                    container.addView(textView);
                    textViewSeason = new TextView(getContext());
                    LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, -6, 0, 0);
                    textViewSeason.setLayoutParams(lp);
                    textViewSeason.setTextColor(style.getMarkedTextColor());
                    textViewSeason.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
                    container.addView(textViewSeason);
                    gradientDrawable = new GradientDrawable(style.getOrientation(), style.getGradientColors());
                    gradientDrawable.setShape(GradientDrawable.OVAL);
                    container.setBackgroundDrawable(gradientDrawable);
                    this.addView(container);
                }
                return;
            case MarkStyle.BACKGROUND:
                this.setLayoutParams(matchParentParams);
                this.setOrientation(HORIZONTAL);
                textView.setTextColor(Color.WHITE);
                circleDrawable = new ShapeDrawable(new OvalShape());
                circleDrawable.getPaint().setColor(style.getColor());
                this.setPadding(20, 20, 20, 20);
                textView.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, (float) 1.0));
                textView.setBackgroundDrawable(circleDrawable);
                this.addView(textView);
                return;
            case MarkStyle.DOT:
                this.setLayoutParams(matchParentParams);
                this.setOrientation(VERTICAL);
                textView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 2.0));
                this.addView(new PlaceHolderVertical(getContext()));
                this.addView(textView);
                this.addView(new Dot(getContext(), style.getColor()));
                return;
            case MarkStyle.RIGHTSIDEBAR:
                this.setLayoutParams(matchParentParams);
                this.setOrientation(HORIZONTAL);
                textView.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, (float) 3.0));
                this.addView(new PlaceHolderHorizontal(getContext()));
                this.addView(textView);
                PlaceHolderHorizontal barRight = new PlaceHolderHorizontal(getContext());
                barRight.setBackgroundColor(style.getColor());
                this.addView(barRight);
                return;
            case MarkStyle.LEFTSIDEBAR:
                this.setLayoutParams(matchParentParams);
                this.setOrientation(HORIZONTAL);
                textView.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, (float) 3.0));
                PlaceHolderHorizontal barLeft = new PlaceHolderHorizontal(getContext());
                barLeft.setBackgroundColor(style.getColor());
                this.addView(barLeft);
                this.addView(textView);
                this.addView(new PlaceHolderHorizontal(getContext()));
                return;
            default:
                throw new IllegalArgumentException("Invalid Mark Style Configuration!");
        }
    }

    @Override
    public void setDisplayText(DayData day) {
        initLayoutWithStyle(day.getDate().getMarkStyle());
        textView.setText(day.getText());
        if (CellConfig.isShowSeasonText && textViewSeason != null) {
            if (!TextUtils.isEmpty(day.getSeasonText())) {
                textViewSeason.setVisibility(View.VISIBLE);
                textViewSeason.setText(day.getSeasonText());
            } else {
                textViewSeason.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setTextViewSeason(DayData day) {

    }

    class PlaceHolderHorizontal extends View {

        LayoutParams params;

        public PlaceHolderHorizontal(Context context) {
            super(context);
            params = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, (float) 1.0);
            this.setLayoutParams(params);
        }

        public PlaceHolderHorizontal(Context context, AttributeSet attrs) {
            super(context, attrs);
            params = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, (float) 1.0);
            this.setLayoutParams(params);
        }
    }

    class PlaceHolderVertical extends View {

        LayoutParams params;

        public PlaceHolderVertical(Context context) {
            super(context);
            params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 1.0);
            this.setLayoutParams(params);
        }

        public PlaceHolderVertical(Context context, AttributeSet attrs) {
            super(context, attrs);
            params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 1.0);
            this.setLayoutParams(params);
        }
    }

    class Dot extends RelativeLayout {

        public Dot(Context context, int color) {
            super(context);
            this.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 1.0));
            View dotView = new View(getContext());
            LayoutParams lp = new RelativeLayout.LayoutParams(10, 10);
            lp.addRule(CENTER_IN_PARENT, TRUE);
            dotView.setLayoutParams(lp);
            ShapeDrawable dot = new ShapeDrawable(new OvalShape());

            dot.getPaint().setColor(color);
            dotView.setBackgroundDrawable(dot);
            this.addView(dotView);
        }
    }
}
