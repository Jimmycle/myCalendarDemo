package com.example.jimmy.mycalendardemo.cal.base;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

public class MarkStyle {
    public static final int BACKGROUND = 1;
    public static final int DOT = 2;
    public static final int LEFTSIDEBAR = 3;
    public static final int RIGHTSIDEBAR = 4;
    public static final int TEXT = 5;
    public static final int Gradient_BACKGROUND = 9;
    public static final int DEFAULT = 10;

    public static int defaultColor = Color.rgb(0, 148, 243);

    public static String text;

    public static int current = DEFAULT;

    private int style;
    private int color;
    private int[] gradientColors;
    private GradientDrawable.Orientation orientation = GradientDrawable.Orientation.TOP_BOTTOM;
    private int defaultTextColor;
    private int markedTextColor;
    private int weekendTextColor;
    private int disableTextColor;

    public MarkStyle() {
        this.style = MarkStyle.DEFAULT;
        this.color = MarkStyle.defaultColor; //背景色
        this.defaultTextColor = Color.BLACK; //默认字体色
        this.markedTextColor = Color.WHITE; //选中字体色
        this.weekendTextColor = 0xFFFFC514; //周末字体色
        this.disableTextColor = 0xFF989898; //静用字体色
    }

    public int getStyle() {
        return style;
    }

    public MarkStyle setStyle(int style) {
        this.style = style;
        return this;
    }

    public int getColor() {
        return color;
    }

    public MarkStyle setColor(int color) {
        this.color = color;
        return this;
    }

    public int getDefaultTextColor() {
        return defaultTextColor;
    }

    public MarkStyle setDefaultTextColor(int defaultTextColor) {
        this.defaultTextColor = defaultTextColor;
        return this;
    }

    public int getMarkedTextColor() {
        return markedTextColor;
    }

    public MarkStyle setMarkedTextColor(int markedTextColor) {
        this.markedTextColor = markedTextColor;
        return this;
    }

    public MarkStyle setWeekendTextColor(int weekendTextColor) {
        this.weekendTextColor = weekendTextColor;
        return this;
    }

    public MarkStyle setDisableTextColor(int disableTextColor) {
        this.disableTextColor = disableTextColor;
        return this;
    }

    public int getWeekendTextColor() {
        return weekendTextColor;
    }

    public int getDisableTextColor() {
        return disableTextColor;
    }

    public MarkStyle setGradientColors(int[] gradientColors) {
        this.gradientColors = gradientColors;
        return this;
    }

    public int[] getGradientColors() {
        return gradientColors;
    }

    public GradientDrawable.Orientation getOrientation() {
        return orientation;
    }

    public MarkStyle setOrientation(GradientDrawable.Orientation orientation) {
        this.orientation = orientation;
        return this;
    }
}
