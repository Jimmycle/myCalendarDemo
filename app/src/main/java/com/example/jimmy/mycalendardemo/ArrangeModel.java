package com.example.jimmy.mycalendardemo;

/**
 * 时间选择model
 */
public class ArrangeModel {
    private String times;
    private int status; //0:enable, 1:disable
    private String state;

    public ArrangeModel(String times, int status, String state) {
        this.times = times;
        this.status = status;
        this.state = state;
    }

    public ArrangeModel(String times, int status) {
        this.times = times;
        this.status = status;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "ArrangeModel{" +
                "times='" + times + '\'' +
                ", state='" + status + '\'' +
                '}';
    }
}
