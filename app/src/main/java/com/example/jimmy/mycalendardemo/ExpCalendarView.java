package com.example.jimmy.mycalendardemo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jimmy.mycalendardemo.cal.adapter.CalendarViewExpAdapter;
import com.example.jimmy.mycalendardemo.cal.base.CellConfig;
import com.example.jimmy.mycalendardemo.cal.base.DateData;
import com.example.jimmy.mycalendardemo.cal.base.MarkStyle;
import com.example.jimmy.mycalendardemo.cal.base.MarkedDates;
import com.example.jimmy.mycalendardemo.cal.listeners.OnDateClickListener;
import com.example.jimmy.mycalendardemo.cal.listeners.OnMonthScrollListener;
import com.example.jimmy.mycalendardemo.cal.util.CurrentCalendar;
import com.example.jimmy.mycalendardemo.cal.util.ExpCalendarUtil;

import java.util.Calendar;
import java.util.Locale;

/**
 * 自定义日历控件视图绘制
 */
public class ExpCalendarView extends ViewPager {
    private static final String TAG = "ExpCalendarView";
    private boolean isInit; //是否初始化

    private DateData currentDate;
    private CalendarViewExpAdapter adapter;

    private DateData mLimitDate;
    private TextView tvShowMonth;

    public ExpCalendarView(Context context) {
        this(context, null);
    }

    public ExpCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof FragmentActivity) {
            init((FragmentActivity) context);
        }
    }

    /**
     * 初始化
     */
    private void init(FragmentActivity activity) {
        if (isInit) {
            return;
        }
        isInit = true;
        if (currentDate == null) {
            currentDate = CurrentCalendar.getCurrentDateData();
        }
        if (this.getId() == View.NO_ID) {
            this.setId(R.id.calendarViewPager);
        }
        adapter = new CalendarViewExpAdapter(activity.getSupportFragmentManager()).setDate(currentDate);
        this.setAdapter(adapter);

        this.setCurrentItem(500);
    }

    /**
     * 设置today为特殊名字
     *
     * @param specialName 特殊名字，比如'今'
     */
    public void setCurrentDateSpecialName(String specialName) {
        currentDate.setCurrentDateSpecialName(specialName);
        adapter.notifyDataSetChanged();
    }

    /**
     * 返回上月功能
     */
    public void travelToPreMonth() {
        if (canBack()) {
            int orgPosition = getCurrentItem();
            int movePosition = orgPosition - 1;
            DateData orgDate = ExpCalendarUtil.position2Week(orgPosition);
            DateData preDate = ExpCalendarUtil.position2Week(movePosition);
            Calendar calendar = Calendar.getInstance();
            calendar.set(preDate.getYear(), preDate.getMonth() - 1, preDate.getDay());
            int w = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            if (w == 0) {
                w = 7;
            }
            calendar.add(Calendar.DAY_OF_MONTH, -w + 1); //获取当前星期一时间
            DateData preDateStart = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            Log.d(TAG, "preDateStart1::" + preDateStart);
            while (orgDate.getMonth() == preDate.getMonth()) {
                Log.d(TAG, "preDateStart2::" + preDateStart);
                if (mLimitDate != null && preDateStart.compareTo(mLimitDate) <= 0) {
                    break;
                }
                movePosition -= 1;
                preDate = ExpCalendarUtil.position2Week(movePosition);
                calendar.add(Calendar.DAY_OF_MONTH, -7);
                preDateStart = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            }
            setCurrentItem(movePosition);
            if (tvShowMonth != null) {
                updateMonthShow(tvShowMonth, ExpCalendarUtil.position2Week(getCurrentItem()));
            }
        }
    }

    /**
     * 进入下月功能
     */
    public void travelToNextMonth() {
        int orgPosition = getCurrentItem();
        int movePosition = orgPosition + 1;
        DateData orgDate = ExpCalendarUtil.position2Week(orgPosition);
        DateData nextDate = ExpCalendarUtil.position2Week(movePosition);
        while (orgDate.getMonth() == nextDate.getMonth()) {
            movePosition += 1;
            nextDate = ExpCalendarUtil.position2Week(movePosition);
        }
        setCurrentItem(movePosition);
        if (tvShowMonth != null) {
            updateMonthShow(tvShowMonth, nextDate);
        }
    }

    /**
     * 返回上星期功能
     */
    public void travelToPreWeek() {
        if (canBack()) {
            setCurrentItem(getCurrentItem() - 1);
            DateData dateData = ExpCalendarUtil.position2Week(getCurrentItem());
            if (tvShowMonth != null) {
                updateMonthShow(tvShowMonth, dateData);
            }
        }
    }

    /**
     * 进入下星期功能
     */
    public void travelToNextWeek() {
        setCurrentItem(getCurrentItem() + 1);
        DateData dateData = ExpCalendarUtil.position2Week(getCurrentItem());
        if (tvShowMonth != null) {
            updateMonthShow(tvShowMonth, dateData);
        }
    }

    /**
     * 禁止返回设定的时间之前
     *
     * @param year  年
     * @param month 月
     * @param day   日
     */
    public void setLimitBackDate(int year, int month, int day) {
        mLimitDate = new DateData(year, month, day);
        if (mLimitDate.compareTo(currentDate) > 0) {
            Toast.makeText(getContext(), "设置禁止返回时间不能大于今天，设置失败", Toast.LENGTH_LONG).show();
            mLimitDate = null;
        }
    }

    /**
     * 限制返回上月上星期
     */
    private boolean canBack() {
        if (mLimitDate == null)
            return true;
        DateData dateData = ExpCalendarUtil.position2Week(getCurrentItem());
        Calendar calendar = Calendar.getInstance();
        calendar.set(dateData.getYear(), dateData.getMonth() - 1, dateData.getDay());
        int w = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (w == 0) {
            w = 7;
        }
        calendar.add(Calendar.DAY_OF_MONTH, -w + 1); //获取当前星期一时间
        DateData dateDataStart = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        if (dateDataStart.compareTo(mLimitDate) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 设置限制最小日期
     *
     * @param year  年
     * @param month 月
     * @param day   日
     */
    public void setMinDate(int year, int month, int day) {
        DateData dateData = new DateData(year, month, day);
        CellConfig.minDate = dateData;
    }

    /**
     * 设置限制最大日期
     *
     * @param year  年
     * @param month 月
     * @param day   日
     */
    public void setMaxDate(int year, int month, int day) {
        DateData dateData = new DateData(year, month, day);
        CellConfig.maxDate = dateData;
    }

    /**
     * 设置不可选日期
     *
     * @param dates 不可选日期,可设置多个时间
     */
    public void setDisableDate(DateData... dates) {
        CellConfig.disableDate = dates;
    }

    /**
     * 设置标记的日期
     *
     * @param year  年
     * @param month 月
     * @param day   日
     */
    public DateData markDate(int year, int month, int day) {
        DateData dateData = new DateData(year, month, day);
        return markDate(dateData);
    }

    /**
     * 设置标记的日期
     *
     * @param dateData DateData基础类
     */
    public DateData markDate(DateData dateData) {
        MarkedDates.getInstance().add(dateData);
        return dateData;
    }

    /**
     * 获取标记的日期
     */
    public MarkedDates getMarkedDates() {
        return MarkedDates.getInstance();
    }

    /**
     * 设置默认日历的字体颜色
     *
     * @param dateData  DateData基础类
     * @param textColor 字体颜色
     */
    public ExpCalendarView setDefaultTextColor(DateData dateData, int textColor) {
        dateData.getMarkStyle().setDefaultTextColor(textColor);
        return this;
    }

    /**
     * 设置标记日历的背景色
     *
     * @param dateData DateData基础类
     * @param color    背景色
     */
    public ExpCalendarView setMarkedColor(DateData dateData, int color) {
        dateData.getMarkStyle().setColor(color);
        return this;
    }

    /**
     * 设置标记日历的字体颜色
     *
     * @param dateData  DateData基础类
     * @param textColor 字体颜色
     */
    public ExpCalendarView setMarkedTextColor(DateData dateData, int textColor) {
        dateData.getMarkStyle().setMarkedTextColor(textColor);
        return this;
    }

    /**
     * 设置日历周末字体颜色
     *
     * @param dateData  DateData基础类
     * @param textColor 字体颜色
     */
    public ExpCalendarView setWeekendTextColor(DateData dateData, int textColor) {
        dateData.getMarkStyle().setWeekendTextColor(textColor);
        return this;
    }

    /**
     * 设置日历不可点击字体颜色
     *
     * @param dateData  DateData基础类
     * @param textColor 字体颜色
     */
    public ExpCalendarView setDisableTextColor(DateData dateData, int textColor) {
        dateData.getMarkStyle().setDisableTextColor(textColor);
        return this;
    }

    /**
     * 设置标记日历背景色为渐变色的颜色数组
     *
     * @param dateData    DateData基础类
     * @param orientation 渐变色方向 {@link android.graphics.drawable.GradientDrawable}.
     * @param colors      颜色集合
     */
    public ExpCalendarView setMarkedGradientColor(DateData dateData, GradientDrawable.Orientation orientation, int[] colors) {
        dateData.getMarkStyle().setStyle(MarkStyle.Gradient_BACKGROUND).setGradientColors(colors).setOrientation(orientation);
        return this;
    }

    /**
     * 是否显示节气开关
     *
     * @param isOpen true表示打开，false表示关闭
     */
    public void setSeasonTextShow(boolean isOpen) {
        CellConfig.isShowSeasonText = isOpen;
    }


    /**
     * 设置滑动日历是的监听接口
     *
     * @param listener 实现接口
     * @return
     */
    public ExpCalendarView setOnMonthScrollListener(final OnMonthScrollListener listener) {
        if (listener != null) {
            this.addOnPageChangeListener(new OnMonthScrollListener() {
                @Override
                public void onMonthChange(int year, int month, int day, int week) {
                    if (tvShowMonth != null) {
                        updateMonthShow(tvShowMonth, new DateData(year, month, day));
                    }
                    listener.onMonthChange(year, month, day, week);
                }

                @Override
                public void onMonthScroll(float positionOffset) {
                    listener.onMonthScroll(positionOffset);
                }
            });
        }
        return this;
    }

    /**
     * 设置点击日期的监听接口
     *
     * @param listener 实现接口
     * @return
     */
    public ExpCalendarView setOnDateClickListener(final OnDateClickListener listener) {
        if (listener != null) {
            OnDateClickListener.instance = new OnDateClickListener() {
                @Override
                public void onDateClick(View view, DateData date, boolean canSelected) {
                    if (tvShowMonth != null) {
                        updateMonthShow(tvShowMonth, date);
                    }
                    listener.onDateClick(view, date, canSelected);
                }
            };
        }
        return this;
    }

    @Override
    protected void onMeasure(int measureWidthSpec, int measureHeightSpec) {
        measureWidthSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(measureWidthSpec), MeasureSpec.EXACTLY);
        measureHeightSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(measureHeightSpec), MeasureSpec.EXACTLY);
        super.onMeasure(measureWidthSpec, measureHeightSpec);
        float density = Resources.getSystem().getDisplayMetrics().density;
        CellConfig.cellWidth = getMeasuredWidth() / 7 / density;
        CellConfig.cellHeight = getMeasuredWidth() / 7 / density;

        int childHeight = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(measureWidthSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            childHeight = child.getMeasuredHeight();
        }
        setMeasuredDimension(getMeasuredWidth(), childHeight);
    }

    public void measureCurrentView() {
        requestLayout();
    }

    boolean isFirst = true;
    int minPage = Integer.MIN_VALUE;
    private int mLastX;

    @Override
    public void scrollTo(int x, int y) {
        Log.d("123", "x::" + x);
        if (!canBack() && isFirst) {
            isFirst = false;
            minPage = getCurrentItem();
        }
        if (mLimitDate == null) {
            super.scrollTo(x, y);
        } else {
            if (mLastX < x) { //允许向右移动
                super.scrollTo(x, y);
                mLastX = x;
            } else {
                if (getCurrentItem() >= minPage) {
                    super.scrollTo(x, y);
                    mLastX = x;
                } else {
                    setCurrentItem(minPage);
                }
            }
        }
    }

    /**
     * 绑定指定年月显示的TextView,动态更新年月
     *
     * @param textView 显示视图控件
     */
    public void bindUpdateTextView(TextView textView) {
        tvShowMonth = textView;
        if (tvShowMonth != null) {
            updateMonthShow(textView, currentDate);
        }
    }

    /**
     * 更新月份显示
     *
     * @param textView 显示视图控件
     * @param dateData 更新的时间设置
     */
    public void updateMonthShow(TextView textView, DateData dateData) {
        String format;
        if (dateData.getMonth() > 9) {
            format = "%d年%d月";
        } else {
            format = "%d年0%d月";
        }
        textView.setText(String.format(Locale.getDefault(), format, dateData.getYear(), dateData.getMonth()));
    }
}

