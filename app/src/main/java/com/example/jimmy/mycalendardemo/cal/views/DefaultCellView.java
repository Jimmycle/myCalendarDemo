package com.example.jimmy.mycalendardemo.cal.views;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jimmy.mycalendardemo.cal.base.CellConfig;
import com.example.jimmy.mycalendardemo.cal.base.DateData;
import com.example.jimmy.mycalendardemo.cal.base.DayData;
import com.example.jimmy.mycalendardemo.cal.base.MarkStyleExp;

import java.util.Calendar;

/**
 * 默认绘制日历格视图
 */
public class DefaultCellView extends BaseCellView {
    public TextView textView;
    private TextView textViewSeason;
    private AbsListView.LayoutParams matchParentParams;

    public DefaultCellView(Context context) {
        super(context);
        initLayout();
    }

    public DefaultCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout();
    }

    private void initLayout() {
        matchParentParams = new AbsListView.LayoutParams((int) CellConfig.cellWidth, (int) CellConfig.cellHeight);
        this.setLayoutParams(matchParentParams);
        this.setOrientation(VERTICAL);
        if (!CellConfig.isShowSeasonText) {
            textView = new TextView(getContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            this.addView(textView);
        } else {
            LinearLayout container = new LinearLayout(getContext());
            container.setOrientation(VERTICAL);
            container.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            container.setGravity(Gravity.CENTER);
            textView = new TextView(getContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            container.addView(textView);
            textViewSeason = new TextView(getContext());
            LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, -6, 0, 0);
            textViewSeason.setLayoutParams(lp);
            textViewSeason.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
            container.addView(textViewSeason);
            this.addView(container);
        }
    }

    @Override
    public void setDisplayText(DayData day) {
        textView.setText(day.getText());
        DateData date = day.getDate();
        setTextColorByDate(textView, date);
    }

    @Override
    public void setTextViewSeason(DayData day) {
        if (!TextUtils.isEmpty(day.getSeasonText())) {
            textViewSeason.setVisibility(View.VISIBLE);
            textViewSeason.setText(day.getSeasonText());
        } else {
            textViewSeason.setVisibility(ViewGroup.GONE);
        }
        setTextColorByDate(textViewSeason, day.getDate());
    }

    public void setTextColorByDate(TextView textView, DateData date) {
        if (isDisableDay(date)) {
            textView.setTextColor(date.getMarkStyle().getDisableTextColor());
        } else {
            if (isWeekWendDay(date)) {
                textView.setTextColor(date.getMarkStyle().getWeekendTextColor());
            } else {
                textView.setTextColor(date.getMarkStyle().getDefaultTextColor());
            }
        }
    }

    public boolean isWeekWendDay(DateData dateData) {
        Calendar c = Calendar.getInstance();
        c.set(dateData.getYear(), dateData.getMonth() - 1, dateData.getDay());
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    public boolean isDisableDay(DateData dateData) {
        if (CellConfig.minDate != null && dateData.compareTo(CellConfig.minDate) < 0) {
            return true;
        }
        if (CellConfig.maxDate != null && dateData.compareTo(CellConfig.minDate) > 0) {
            return true;
        }
        if (CellConfig.disableDate != null) {
            for (int i = 0; i < CellConfig.disableDate.length; i++) {
                if (dateData.equals(CellConfig.disableDate[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void onMeasure(int measureWidthSpec, int measureHeightSpec) {
        super.onMeasure(measureWidthSpec, measureHeightSpec);
    }

    public boolean setDateChoose() {
        setBackgroundDrawable(MarkStyleExp.choose);
        textView.setTextColor(Color.WHITE);
        return true;
    }

    public void setDateToday() {
        setBackgroundDrawable(MarkStyleExp.today);
        textView.setTextColor(Color.rgb(105, 75, 125));
    }

    public void setDateNormal() {
        textView.setTextColor(Color.BLACK);
        setBackgroundDrawable(null);
    }

    public void setTextColor(String text, int color) {
        textView.setText(text);
        if (color != 0) {
            textView.setTextColor(color);
        }
    }

    public void setTextSeasonColor(String text, int color) {
        if (!TextUtils.isEmpty(text)) {
            textViewSeason.setVisibility(View.VISIBLE);
            textViewSeason.setText(text);
            textViewSeason.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
        } else {
            textViewSeason.setVisibility(View.GONE);
        }
        if (color != 0) {
            textViewSeason.setTextColor(color);
        }
    }
}
