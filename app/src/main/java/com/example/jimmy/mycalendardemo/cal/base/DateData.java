package com.example.jimmy.mycalendardemo.cal.base;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.example.jimmy.mycalendardemo.cal.util.CurrentCalendar;
import com.example.jimmy.mycalendardemo.cal.util.DPCNCalendar;

import java.util.Calendar;

/**
 * 日历基础类
 */
public class DateData implements Comparable {
    /**
     * 年
     */
    private int year;
    /**
     * 月
     */
    private int month;
    /**
     * 天
     */
    private int day;
    /**
     * 选中日期样式类
     */
    private MarkStyle markStyle;
    /**
     * 特殊命名称，不如几天标注为天
     */
    private String specialName;
    /**
     * 节气名称
     */
    private String solarTermName;

    /**
     * 构造方法
     *
     * @param year  年
     * @param month 月
     * @param day   日
     */
    public DateData(int year, int month, int day) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.markStyle = new MarkStyle();
        this.solarTermName = new DPCNCalendar().getSolarTerm(year, month, day);
    }

    /**
     * 获取年份
     *
     * @return
     */
    public int getYear() {
        return year;
    }

    /**
     * 设置年份
     *
     * @param year 年
     * @return
     */
    public DateData setYear(int year) {
        this.year = year;
        return this;
    }

    /**
     * 获取月份
     *
     * @return
     */
    public int getMonth() {
        return month;
    }

    /**
     * 设置月份
     *
     * @param month 月
     * @return
     */
    public DateData setMonth(int month) {
        this.month = month;
        return this;
    }

    /**
     * 获取天
     *
     * @return
     */
    public int getDay() {
        return day;
    }

    /**
     * 设置天
     *
     * @param day 天
     * @return
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * 获取标记样式
     *
     * @return
     */
    public MarkStyle getMarkStyle() {
        return markStyle;
    }

    /**
     * 设置标记样式
     *
     * @param markStyle
     * @return
     */
    public DateData setMarkStyle(MarkStyle markStyle) {
        this.markStyle = markStyle;
        return this;
    }

    /**
     * 获取特殊名称
     *
     * @return
     */
    public String getCurrentDateSpecialName() {
        return this.specialName;
    }

    /**
     * 设置特殊名称
     *
     * @param specialName 名称
     */
    public void setCurrentDateSpecialName(String specialName) {
        if (CurrentCalendar.getCurrentDateData().equals(this)) {
            if (!TextUtils.isEmpty(specialName)) {
                this.specialName = specialName;
            }
        }
    }

    /**
     * 获取节气
     *
     * @return
     */
    public String getSolarTermName() {
        return solarTermName;
    }

    @Override
    public boolean equals(Object o) {
        DateData data = (DateData) o;
        return ((data.year == this.year) && (data.month == this.month) && (data.day == this.day));
    }

    @Override
    public String toString() {
        return "DateData{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", specialName=" + specialName +
                ", MarkStyle =" + markStyle +
                ", solarTermName =" + solarTermName +
                '}';
    }

    @Override
    public int compareTo(@NonNull Object o) {
        DateData data = (DateData) o;
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.set(this.getYear(), this.getMonth() - 1, this.getDay());
        c2.set(data.getYear(), data.getMonth() - 1, data.getDay());
        return c1.compareTo(c2);
    }
}
