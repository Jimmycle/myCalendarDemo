package com.example.jimmy.mycalendardemo.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.jimmy.mycalendardemo.R;
import com.example.jimmy.mycalendardemo.WaterImageManager;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

/**
 * Created by jimmy on 2018/5/22.
 */
public class SlideActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
        , DrawerLayout.DrawerListener, View.OnTouchListener {
    private static final String TAG = "SlideActivity";
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private CoordinatorLayout right;
    private NavigationView left;
    private boolean isDrawer;
    private int widthPixels;
    private int heightPixels;
    private ViewGroup rootView;
    private ImageView imageWater;
    private WaterImageManager waterImageManager;
    private int animCount;
    private float startX, startY, lastOffSet;
    private AnimationHandler aHandler;
    private boolean isBreathingLampRunning = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);
        waterImageManager = new WaterImageManager();
        aHandler = new AnimationHandler(this);
        initView();
    }

    private void initView() {
        rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        right = findViewById(R.id.right);
        left = findViewById(R.id.nav_view);

        setSupportActionBar(toolbar);
        DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
        widthPixels = dm.widthPixels;
        heightPixels = dm.heightPixels;

        setDrawerMinMargin(drawer, widthPixels / 4);
        drawer.setScrimColor(Color.parseColor("#33000000"));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //动态添加WaterView
        startX = 0; //设置起始x坐标
        startY = dp2px(62); //设置起始y坐标
        addWaterView(startX, startY, waterImageManager.getBreathingLampImage(0));
        //播放呼吸灯动画
        startBreathingLampAnimation();

        left.setNavigationItemSelectedListener(this);
        right.setOnTouchListener(this);
        drawer.addDrawerListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        isDrawer = true;
        //整体外移
        right.layout(left.getRight(), 0, left.getRight() + widthPixels, heightPixels);
        //停止动画
        cancelBreathingLampAnimation();
        if (lastOffSet < slideOffset) { //opening state
            int viewWidth = drawerView.getMeasuredWidth();
            float x = viewWidth * slideOffset;
            int resId = waterImageManager.getCurrentImageResIdByOffSet(slideOffset);
            transWaterView(x, startY, resId);
        } else if (lastOffSet > slideOffset) { //closing state
            clearWaterView();
            if (slideOffset == 0.0f) {
                //播放呼吸灯动画
                transWaterView(startX, startY, waterImageManager.getBreathingLampImage(0));
                startBreathingLampAnimation();
            }
        } else { //idle state

        }
        lastOffSet = slideOffset;
    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {
//        Log.d(TAG, "onDrawerOpened");
        clearWaterView();
    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {
//        Log.d(TAG, "onDrawerClosed");
    }

    @Override
    public void onDrawerStateChanged(int newState) {
//        Log.d(TAG, "newState-->" + newState);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return isDrawer && left.dispatchTouchEvent(event);
    }

    /**
     * @param drawerLayout
     * @param marginLeft   px
     */
    public void setDrawerMinMargin(DrawerLayout drawerLayout, int marginLeft) {
        try {
            // mMinDrawerMargin 默认是64dp
            Field mMinDrawerMarginField = drawerLayout.getClass().getDeclaredField("mMinDrawerMargin");
            mMinDrawerMarginField.setAccessible(true);
            int minDrawerMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, marginLeft, getResources().getDisplayMetrics());
            mMinDrawerMarginField.set(drawerLayout, minDrawerMargin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addWaterView(float x, float y, int imageId) {
        if (imageWater == null) {
            imageWater = new ImageView(this);
            imageWater.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imageWater.setImageResource(imageId);
            imageWater.setX(x);
            imageWater.setY(y);
            rootView.addView(imageWater);
        }
    }

    public void transWaterView(float x, float y, int imageId) {
        if (imageWater != null) {
            imageWater.setImageResource(imageId);
            imageWater.setX(x);
            imageWater.setY(y);
        }
    }

    public void clearWaterView() {
        if (imageWater != null) {
            imageWater.setImageResource(0);
        }
    }

    public void removeWaterView() {
        if (imageWater != null) {
            rootView.removeView(imageWater);
            imageWater = null;
        }
    }

    public void startBreathingLampAnimation() {
        Message msg = aHandler.obtainMessage();
        msg.what = 0x01;
        aHandler.sendMessage(msg);
        isBreathingLampRunning = true;
    }

    private void periodBreathingLampAnimation() {
        Message msg = aHandler.obtainMessage();
        msg.what = 0x01;
        aHandler.sendMessageDelayed(msg, WaterImageManager.BREATHING_LAMP_PERIOD_TIME);
    }

    public void cancelBreathingLampAnimation() {
        aHandler.removeCallbacksAndMessages(null);
        isBreathingLampRunning = false;
    }

    public class AnimationHandler extends Handler {
        WeakReference<Activity> mActivityReference;

        AnimationHandler(Activity a) {
            mActivityReference = new WeakReference<>(a);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            final Activity a = mActivityReference.get();
            if (a != null) {
                switch (msg.what) {
                    case 0x01:
                        animCount++;
                        if (imageWater != null) {
                            int resId = waterImageManager.getBreathingLampImage(animCount);
                            imageWater.setImageResource(resId);
                        }
                        periodBreathingLampAnimation();
                        // Log.d(TAG, "animCount==" + animCount);
                        break;
                }
            }
        }
    }

    /**
     * 设置开始x位置
     *
     * @param x
     */
    public void setStartX(float x) {
        startX = x;
    }

    /**
     * 设置开始y位置
     *
     * @param y
     */
    public void setStartY(float y) {
        startY = y;
    }

    private int dp2px(int dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, getResources().getDisplayMetrics());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isBreathingLampRunning) {
            startBreathingLampAnimation();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isBreathingLampRunning) {
            cancelBreathingLampAnimation();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelBreathingLampAnimation();
        removeWaterView();
    }
}
