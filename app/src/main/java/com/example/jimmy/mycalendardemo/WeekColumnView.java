package com.example.jimmy.mycalendardemo;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jimmy.mycalendardemo.cal.util.ExpCalendarUtil;

/**
 * 日历标题栏视图绘制
 */
public class WeekColumnView extends LinearLayout {

    private int backgroundColor = Color.WHITE;
    private int textColor = Color.BLACK;
    private int weekendTextColor = Color.BLACK;
    private String[] columnNames;

    private TextView[] textView = new TextView[7];

    public WeekColumnView(Context context) {
        this(context, null);
    }

    public WeekColumnView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeekColumnView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        initLayout();
        initViews();
    }

    private void initLayout() {
        this.setOrientation(HORIZONTAL);
        this.setBackgroundColor(backgroundColor);

    }

    private void initViews() {
        this.removeAllViews();
        LayoutParams lp = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1);
        if (columnNames == null) {
            columnNames = new String[7];
            for (int i = 0; i < 7; i++) {
                columnNames[i] = ExpCalendarUtil.number2Week(i + 1);
            }
        }
        for (int i = 0; i < 7; i++) {
            textView[i] = new TextView(getContext());
            textView[i].setGravity(Gravity.CENTER);
            textView[i].setText(columnNames[i]);
            if (i == 5 || i == 6) {
                textView[i].setTextColor(weekendTextColor);
            } else {
                textView[i].setTextColor(textColor);
            }
            this.addView(textView[i], lp);
        }
    }

    /**
     * 设置日历标题的背景色
     *
     * @param color 颜色
     */
    public void setColumnBackgroundColor(int color) {
        if (color == 0) {
            backgroundColor = Color.WHITE;
        } else {
            backgroundColor = color;
        }
        this.setBackgroundColor(backgroundColor);
    }

    /**
     * 设置日历标题周末的字体颜色
     *
     * @param color 颜色
     */
    public void setColumnWeekendTextColor(int color) {
        if (color == 0) {
            weekendTextColor = Color.BLACK;
        } else {
            weekendTextColor = color;
        }
        initViews();
    }

    /**
     * 设置日历标题的字体颜色
     *
     * @param color 颜色
     */
    public void setColumnTextColor(int color) {
        if (color == 0) {
            textColor = Color.BLACK;
        } else {
            textColor = color;
        }
        initViews();
    }

    /**
     * 设置日历标题栏的数据集
     *
     * @param columnNames 名称数组集合
     */
    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
        initViews();
    }

}
