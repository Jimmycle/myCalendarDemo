# FYX1
### 日历选择控件<br>
##### 无第三方库引入
##### 兼容版本 minSdkVersion >= 16
##### 使用说明
1.初始化日历控件标题栏

```
    public void initWeekColumnView() {
        weekColumnView.setColumnBackgroundColor(Color.WHITE); //背景色，不设置有默认值
        weekColumnView.setColumnTextColor(0xFF323232); //字体颜色，不设置有默认值
        weekColumnView.setColumnWeekendTextColor(0xFFFFC514);//周末颜色，不设置有默认值
        weekColumnView.setColumnNames(getResources().getStringArray(R.array.week_titles)); //初始化标题内容，不设置有默认值
    }
``` 
   
2.初始化日历控件

```    
    public void initCalendarView() {
        Calendar calendar = Calendar.getInstance();
        //绑定月视图,内容实时更新，如果不绑定月视图不会更新
        expCalendarView.bindUpdateTextView(tvYearMonth);
        //当前date显示名字修改
        expCalendarView.setCurrentDateSpecialName("今");
        //选中项设置,当天之后
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        DateData markDate = expCalendarView.markDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        //初始化选中颜色和背景色
        // expCalendarView.setMarkedColor(markDate, Color.parseColor("#FF2DE9FF"));
        expCalendarView.setMarkedTextColor(markDate, Color.WHITE);
        //初始化背景渐变色
        int[] colors = {0xff2DE9FF, 0xff0BABFE}; //渐变色
        expCalendarView.setMarkedGradientColor(markDate, GradientDrawable.Orientation.TOP_BOTTOM, colors);
        //设置默认字体
        expCalendarView.setDefaultTextColor(markDate, 0xFF323232);
        //设置周末字体颜色
        expCalendarView.setWeekendTextColor(markDate, 0xFFFFC514);
        //设置不可点击字体颜色
        expCalendarView.setDisableTextColor(markDate, 0xFF989898);
        //设置最小日期
        // calendar.add(Calendar.DAY_OF_MONTH, -2);
        expCalendarView.setMinDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        //设置最大日期
        // expCalendarView.setMaxDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, calendar.get(Calendar.DAY_OF_MONTH));
        //设置不可选日期
        // DateData date1 = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, 1);
        // DateData date2 = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, 3);
		// DateData date3 = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, 5);
		//  expCalendarView.setDisableDate(date1, date2, date3);
        //禁止向上返回的时间
        calendar = Calendar.getInstance();
        expCalendarView.setLimitBackDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        //是否打开节气显示，默认关闭
        expCalendarView.setSeasonTextShow(true);
    }
```

3.加载上午数据

```     
    private void loadAm() {
        ArrayList<ArrangeModel> data = TestData.initAmData();
        tests.clear();
        tests.addAll(data);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClick(new GridAdapter.OnItemClick() {
            @Override
            public void onClick(View convertView, int position, String times, int status, String state) {
                Log.d(TAG, "，position:" + position + "，times:" + times + "，status:" + status + "，state:" + state);
                adapter.setSelection(position);
            }
        });
    }
```

4.加载下午数据

```    
    private void loadPm() {
        ArrayList<ArrangeModel> data = TestData.initPmData();
        tests.clear();
        tests.addAll(data);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClick(new GridAdapter.OnItemClick() {
            @Override
            public void onClick(View convertView, int position, String times, int status, String state) {
                Log.d(TAG, "，position:" + position + "，times:" + times + "，status:" + status + "，state:" + state);
                adapter.setSelection(position);
            }
        });
    }
```

5.测试数据的初始化  

```
public static ArrayList<ArrangeModel> initAmData() {
        ArrayList<ArrangeModel> test = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 8);
        c.set(Calendar.MINUTE, 15);
        String format;
		//显示格式调整
        for (int i = 0; i < 14; i++) {
            c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 15);
            if (c.get(Calendar.HOUR) > 9) {
                if (c.get(Calendar.MINUTE) > 9) {
                    format = "%d:%d";
                } else {
                    format = "%d:0%d";
                }
            } else {
                if (c.get(Calendar.MINUTE) > 9) {
                    format = "0%d:%d";
                } else {
                    format = "0%d:0%d";
                }
            }
            String time = String.format(Locale.getDefault(), format, c.get(Calendar.HOUR), c.get(Calendar.MINUTE));
            ArrangeModel model;
            //状态调整
            if (i == 13) {
                model = new ArrangeModel(time, 0, "闲");
            } else if (i == 10) {
                model = new ArrangeModel(time, 1, "满");
            } else {
                model = new ArrangeModel(time, 0, "");
            }
            test.add(model);
        }
        return test;
    }
```      

6.关于监听设置

``` 
		/**
         * 注册月份滚动监听
         */
        expCalendarView.setOnMonthScrollListener(new OnMonthScrollListener() {
            @Override
            public void onMonthChange(int year, int month, int date, int week) {
                Log.d(TAG, "，year:" + year + "，month:" + month + "，date:" + date + "，week:" + week);
            }

            @Override
            public void onMonthScroll(float positionOffset) {
            }
        });

        /**
         * 注册日期选择监听
         */
        expCalendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date, boolean canSelected //是否可以进行选择，由设置最大最小值决定
            ) {
                if (canSelected) {
                    expCalendarView.getMarkedDates().removeAdd();
                    expCalendarView.markDate(date); //只标记选中日期
                    expCalendarView.setMarkedTextColor(date, Color.WHITE);
                    //初始化背景渐变色
                    int[] colors = {0xff2DE9FF, 0xff0BABFE}; //渐变色
                    expCalendarView.setMarkedGradientColor(date, GradientDrawable.Orientation.TOP_BOTTOM, colors);
                } else {
                    Toast.makeText(getApplicationContext(), "不能选择该日期", Toast.LENGTH_SHORT).show();
                }
            }
        }); 
        
 	 	/**
    	 * 按钮监听
     	 */        
		preMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToPreMonth(); //进入上月
            }
        });

        nextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToNextMonth();  //进入下月
            }
        });

        preWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToPreWeek(); //进入上周
            }
        });

        nextWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expCalendarView.travelToNextWeek(); //进入下周
            }
        });          
```   

   
  
 
  
  


  

  

  







