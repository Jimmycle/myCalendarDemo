package com.example.jimmy.mycalendardemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * 日程安排适配器
 */
public class GridAdapter extends BaseAdapter {
    ArrayList<ArrangeModel> modelList;
    Context context;
    LayoutInflater mInflater;
    int selectionItem = -1;
    private OnItemClick onItemClick;

    public GridAdapter(Context context, ArrayList<ArrangeModel> modelList) {
        this.context = context;
        this.modelList = modelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return modelList == null ? 0 : modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ItemViewTag viewTag;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item, null);
            // construct an item tag
            viewTag = new ItemViewTag((TextView) convertView.findViewById(R.id.content), (TextView) convertView.findViewById(R.id.status));
            convertView.setTag(viewTag);
        } else {
            viewTag = (ItemViewTag) convertView.getTag();
        }
        final ArrangeModel data = modelList.get(position);
        viewTag.content.setText(data.getTimes());
        if (data.getStatus() == 0) { //状态控制
            if (selectionItem == position) {//颜色控制
                viewTag.content.setBackgroundResource(R.drawable.grid_item_status_select);
                viewTag.content.setTextColor(0xFF0BABFE);
                viewTag.status.setTextColor(0xFF0BABFE);
            } else {
                viewTag.content.setBackgroundResource(R.drawable.grid_item_status_normal);
                viewTag.content.setTextColor(0xFF323232);
                viewTag.status.setTextColor(0xFF323232);
            }
        } else if (data.getStatus() == 1) {
            viewTag.content.setBackgroundResource(R.drawable.grid_item_status_disable);
            viewTag.content.setTextColor(0xFFEBEBEB);
            viewTag.status.setTextColor(0xFFEBEBEB);
        }
        viewTag.status.setText(data.getState());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null) {
                    onItemClick.onClick(v, position, data.getTimes(), data.getStatus(), data.getState());
                }
            }
        });

        return convertView;
    }

    class ItemViewTag {
        protected TextView content;
        protected TextView status;

        public ItemViewTag(TextView content, TextView status) {
            this.content = content;
            this.status = status;
        }
    }

    public void setSelection(int position) {
        selectionItem = position;
        this.notifyDataSetChanged();
    }

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface OnItemClick {
        void onClick(View convertView, int position, String times, int status, String state);
    }
}

